<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8" />
<title>Player - Rádio Play Zone</title>
<link href="css/css.css" type="text/css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
<script type="text/javascript" src="js/player.js"></script>
<script type="text/javascript" src="js/soundmanager2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
</head>

<body onload="document.getElementById('patrickPlayer').play();">
<div id="player-topo">
	<div class="head">
    	<div id="lm"></div>
        <a href="http://radioplayzone.com.br" target="_blank"><div id="logo-playzone"></div></a>
        <div id="box-play-pause">
        	<div id="amount" style="width:0px; height:0px;"></div>
            <div onclick="document.getElementById('patrickPlayer').play();$('#stopBtn').show();$(this).hide();" id="playBtn"></div>
			<div onclick="document.getElementById('patrickPlayer').pause();$('#playBtn').show();$(this).hide();" id="stopBtn"></div>
        </div>
        <div id="info-redes">
        	<a href="http://facebook.com/radioplayzone" target="_blank"><div id="facebook"></div></a>
            <a href="http://twitter.com/playzonebr" target="_blank"><div id="twitter"></div></a>
        </div>
    </div>
</div>

        <?php  require_once '../player.php'; ?>
</body>
</html>