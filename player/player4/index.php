<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8" />
<title>Player - Rádio Play Zone</title>
<link href="css/css.css" type="text/css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
<script type="text/javascript" src="js/player.js"></script>
<script type="text/javascript" src="js/soundmanager2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
</head>

<body onload="document.getElementById('patrickPlayer').play();">
<div id="player-topo">
	<div class="head">
        <a href="http://radioplayzone.com.br" target="_blank"><div id="logo-playzone"></div></a>
        <div id="box-play-pause">
        	<div id="amount" style="width:0px; height:0px;"></div>
            <div onclick="document.getElementById('patrickPlayer').play();$('#stopBtn').show();$(this).hide();" id="playBtn"></div>
            <div onclick="document.getElementById('patrickPlayer').pause();$('#playBtn').show();$(this).hide();" id="stopBtn"></div>
        </div>
        <div id="foto"></div>
        <div id="site"></div>
    </div>
</div>

        <?php  require_once '../player.php'; ?>
</body>
</html>