<?php
session_start();
if(!$_SESSION['user']){
	header('Location: login.php');
} ?><!DOCTYPE html>
<html ng-app="pz">
<head>
	<meta charset="utf-8" />
	<base href='/' />
	<title>Painel Administrável</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular-route.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular-animate.min.js"></script>

	<!--editor -->
	
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js" type="text/javascript"></script>
		<script src="http://textangular.com/dist/textAngular-rangy.min.js" type="text/javascript"></script>
		<script src="http://textangular.com/dist/textAngular-sanitize.min.js" type="text/javascript"></script>
		<script src="http://textangular.com/dist/textAngular.min.js" type="text/javascript"></script>

</head>
<body >
<div class="top">
	<div class="center">
		<span> Seja bem vindo, <b>{{user.nome}}</b> </span>
		<div class="photo" style="background: url('images/photos/{{user.id}}.jpg')center;background-size: cover"></div>
	</div>
</div><div class="menu-left">
	<div class="menu-top">
		<div class="photo" style="background: url('images/photos/{{user.id}}.jpg')center;background-size: cover"></div>
		<div class="name">{{user.nome}} {{user.sobrenome}}</div>
		<div class="rank">{{user.rank}}</div>
	</div>
	<ul>
		<a href="aa/.."><li><ic></ic><span>Pagina Inicial</span> </li></a>
		<a href="/edit_profile"><li><ic></ic><span>Editar perfil</span> </li></a>
		<a ng-if="user.rank=='Administrador'" href="/frases_top"><li><ic></ic><span>Frases (aviso)</span> </li></a>
		<a ng-if="user.rank=='Administrador'" href="/radio_config"><li><ic></ic><span>Configurações da rádio</span> </li></a>
		<a ng-if="user.rank=='Administrador'" href="/emails"><li><ic></ic><span>Email's de contato</span></li></a>
		<a ng-if="user.rank=='Administrador'" href="/mais_tocadas"><li><ic></ic><span>Mais Tocadas</span> </li></a>
		<a ng-if="user.rank=='Administrador'" href="/top_nacional"><li><ic></ic><span>Top Nacional</span> </li></a>
		<a ng-if="user.rank=='Administrador' || user.rank=='Diretor de Parcerias'" href="/parceiros"><li><ic></ic><span>Parceiros</span> </li></a>
		<a ng-if="user.rank=='Administrador'" href="/programacao"><li><ic></ic><span>Programação</span> </li></a>
		<a ng-if="user.rank=='Administrador'" href="/slide"><li><ic></ic><span>Slide</span> </li></a>
		<a ng-if="user.rank=='Administrador' || user.rank=='Locutor'" href="/pedidos"><li><ic></ic><span>Pedidos</span> </li></a>
	</ul>
</div>

<div class="ng-view content"></div>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/controllers.js"></script>
<div id="user" style="display:none"><?php echo$_SESSION['user']; ?></div>
</body>
</html>