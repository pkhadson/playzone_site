var app = angular.module("pz",['ngRoute','ngAnimate','textAngular'])
.config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
  $routeProvider
  .when('/home', {templateUrl: 'views/home.html',controller:"HomeCtrl"})
  .when('/edit_profile', {templateUrl: 'views/edit_profile.html',controller:"EditProfileCtrl"})
  .when('/frases_top', {templateUrl: 'views/frases_top.html',controller:"FrasesTopCtrl"})
  .when('/radio_config', {templateUrl: 'views/radio_config.html',controller:"RadioConfigCtrl"})
  .when('/emails', {templateUrl: 'views/emails.html',controller:"EmailsCtrl"})
  .when('/mais_tocadas', {templateUrl: 'views/mais_tocadas.html',controller:"MaisTocadasCtrl"})
  .when('/top_nacional', {templateUrl: 'views/top_nacional.html',controller:"TopNacionalCtrl"})
  .when('/parceiros', {templateUrl: 'views/parceiros.html',controller:"ParceirosCtrl"})
  .when('/programacao', {templateUrl: 'views/programacao.html',controller:"ProgramacaoCtrl"})
  .when('/slide', {templateUrl: 'views/slide.html',controller:"SlideCtrl"})
  .when('/pedidos', {templateUrl: 'views/pedidos.html',controller:"PedidosCtrl"})

  .otherwise({redirectTo: '/home'});
}).run(function($rootScope, $window, $location){
	$rootScope.location=$location;
	$rootScope.user=JSON.parse(document.getElementById('user').innerHTML);
})
.filter('urlencode', [function() {
  var a = window.encodeURIComponent;
  return a;
}])
.filter('booln', function() {
    return function(input) {
        return input ? 1 : 0;
    }
})