app.controller("HomeCtrl", function($scope,$http){
	$scope.noar={"music":"Carregando..","programa":"Carregando.."};
	$scope.loadNoar = function (){
		$scope.noar={"music":"Carregando..","programa":"Carregando.."};
		$http.get('php_files/no_ar.php').then(function success(data){
			$scope.noar = data.data;
		});
	};
	$scope.loadNoar();
	$scope.loadFalaae = function(){
		$http.get('php_files/falaae.php').then(function success(data){
			$scope.falaae = data.data;
		});
	};$scope.loadFalaae();
	$scope.funcFalaae = function(id,t){
		if(t==0 && !confirm('Deseja mesmo excluir?')){
			return 0;
		}
		$http.post('php_files/falaae_func.php', {'id':id,'event':t}).then(function success(data){
			$scope.loadFalaae();
		});
	}
	$scope.ifDataFalaae=false;
	$scope.ifFalaae = function (stat){
		if($scope.ifDataFalaae){
			return true;
		}else{
			if(stat==0){
				return true;
			}else{
				return false;
			}
		}
	}
	$scope.setDataFalaae = function (data){
		$scope.ifDataFalaae=data;
	}
})
.controller("EditProfileCtrl", function($scope,$http,$rootScope){
	$scope.user_edit=$rootScope.user;
	$scope.submitEditProfile = function (data){
		alert('Aguarde...');
		var files = document.getElementById('ooi').files[0];
		if (!files) {
    		$http.post('php_files/edit_profile.php', data).then(function success(data){
  				alert('alterado com sucesso');
  				console.log(data.data);
  			});
  		}else{
  			var reader = new FileReader();
			reader.readAsDataURL(files);
			reader.onload = function () {
				data.image=reader.result;
  				$http.post('php_files/edit_profile.php', data).then(function success(data){
  					alert('alterado com sucesso');
  					console.log(data.data);
  				});
			};
			reader.onerror = function (error) {
				alert('Ocorreu um erro!');
		   };
  		}
	}
})
.controller("FrasesTopCtrl", function($scope,$http,$rootScope){
	$scope.loadFrasesTop = function (){
		$http.get('php_files/frases_top.php').then(function success(data){
			$scope.frases_top = data.data;
		});
	}
	$scope.loadFrasesTop();
	$scope.editFrase = function (id,frase){
		$scope.showForm=id;
		$scope.frase_edit=frase.frase;
	}
	$scope.editFraseSubmit = function (id,new_frase){
		if(confirm('Tem certeza que deseja alterar?')){
			$http.post('php_files/set_frase.php', {'id':id, 'new_frase':new_frase}).then(function success(data){
				alert('Alterado com sucesso');
				$scope.showForm=-4681;
				$scope.loadFrasesTop();
			});
		}
	}
	$scope.deleteFrase = function (id){
		if(confirm('Tem certeza que deseja excluir?')){
			$http.post('php_files/delete_frase.php', {'id':id}).then(function success(data){
				alert('Excluido com sucesso');
				$scope.showForm=-4681;
				$scope.loadFrasesTop();
			});
		}
	}
	$scope.insertFrase = function (frase){
		if(confirm('Deseja mesmo criar uma nova frase?')){
			$http.post('php_files/insert_frase.php', {'frase':frase}).then(function success(data){
				alert('Frase inserida com sucesso!');
				$scope.loadFrasesTop();
				delete $scope.insert_frase;
			});
		}
	}
})
//.controller("RadioConfigCtrl", function($scope,$http,$rootScope,textAngularManager){
.controller("RadioConfigCtrl", ['$scope','$http', 'textAngularManager', function ($scope, $http, textAngularManager){
	//$('textarea').froalaEditor();
	$scope.loadProgramas = function (){
		$http.get('php_files/programas.php').then(function success(data){
			$scope.programas = data.data;
		});
	}
	$scope.loadProgramas();

	$scope.loadRadioConfig = function (){
		$http.get('php_files/radio_config.php').then(function success(data){
			$scope.radioconfig = data.data;
			$scope.radioconfig.porta=parseInt($scope.radioconfig.porta);
		});
	}
	$scope.loadRadioConfig();
	$scope.submitRadioConfig = function (data_form){
		$http.post('php_files/set_radio_config.php',data_form).then(function success(data){
			alert('Alterado com sucesso!');
			console.log(data.data);
		})
	}
$scope.version = textAngularManager.getVersion();
        $scope.versionNumber = $scope.version.substring(1);
		$scope.htmlContent = '<h2>Try me!</h2><p>textAngular is a super cool WYSIWYG Text Editor directive for AngularJS</p><p><b>Features:</b></p><ol><li>Automatic Seamless Two-Way-Binding</li><li style="color: blue;">Super Easy <b>Theming</b> Options</li><li>Simple ffdfffgEditor Instance Creation</li><li>Safely Parses Html for Custom Toolbar Icons</li><li>Doesn&apos;t Use an iFrame</li><li>Works with Firefox, Chrome, and IE8+</li></ol><p><b>Code at GitHub:</b> <a href="https://github.com/fraywing/textAngular">Here</a> </p>';

}])
.controller("EmailsCtrl", function($scope,$http,$rootScope){
	$scope.typeread=false;
	$scope.loadEmails = function (){
		$http.get('php_files/emails.php').then(function success(data){
			$scope.emails=data.data;
			if($scope.emails.results.length!=0){console.log('tem');
				if($scope.emails.count==0){
					$scope.typeread=true;
				}
			}
		})
	}
	$scope.loadEmails();
	$scope.setRead = function (id, type){
		if(type==0){
			if(!confirm('Deseja mesmo marcar como não-lido?')){return 0}
		}else{
			if(!confirm('Deseja mesmo marcar como lido?')){return 0}
		}
		$http.post('php_files/set_email.php', {'id':id, 'stat':type}).then(function sucess(){
			alert('Alterado com sucesso!');
			$scope.loadEmails();
		});
	}
})
.controller("MaisTocadasCtrl", function($scope,$http,$rootScope){
	$scope.loadMusic = function (){
		$http.get('php_files/mais_tocadas.php').then(function success(data){
			$scope.musics=data.data;
		});
	}
	$scope.loadMusic();
	$scope.funcShowForm = function (id){
		$scope.showForm =id.id;
		$scope.editM=id;
		$scope.editM.search=id.music + ' - '+ id.artist;
		$scope.editM.image='http://radioplayzone.com/images/mais_tocadas/'+id.id+'.jpg';
	}
	$scope.submitEditMusic = function (data){
		if(data.showForm){
			$http.post('php_files/mais_tocadas_search.php',data).then(function success(data){
				console.log(data.data);
				if(data.data=='error'){
					alert('Desculpe! Ocorreu um erro!');
					delete $scope.editM.showForm;
				}
				if(data.data.error==1){
					alert('Desculpe! Ocorreu um erro!\nMas a pesquisa foi feita');
					delete $scope.editM.showForm;
				}
				if(data.data.music){
					$scope.editM=data.data;
					$scope.editM.id=data.data.id;
					$scope.editM.search=data.data.music + ' - '+ data.data.artist;
					alert('Pesquisa concluida! \n Caso a pesquisa ter dado errado, altere-o');
				}
			});
		}else{
			var files = document.getElementById('image').files[0];
			if (!files) {
  				$http.post('php_files/set_mais_tocadas.php', data).then(function success(data){
  					alert('alterado com sucesso');
  					$scope.showForm=0;
  					console.log(data.data);
  				});
  			}else{
  				var reader = new FileReader();
				reader.readAsDataURL(files);
				reader.onload = function () {
					data.image=reader.result;
  					$http.post('php_files/set_mais_tocadas.php', data).then(function success(data){
  						alert('alterado com sucesso');
  						$scope.showForm=0;
  						console.log(data.data);
  					});
				};
				reader.onerror = function (error) {
					alert('Ocorreu um erro!');
		   		};
  			}
		}
	}
})
.controller("TopNacionalCtrl", function($scope,$http,$rootScope){
	$scope.loadMusics=function (){
		$http.get('php_files/top_nacional.php').then(function success(data){
			$scope.musics=data.data;
		});
	}
	$scope.loadMusics();
	$scope.funcShowForm = function (id){
		$scope.showForm =id.id;
		$scope.editM=id;
	}
	$scope.submitMusic = function (data){
		$http.post('php_files/set_top_nacional.php', data).then(function success(data){
			console.log(data.data);
			alert('Alterado com sucesso!');
			delete $scope.showForm;
			delete $scope.editM;
			$scope.loadMusics();
		});
	}
})
.controller("ParceirosCtrl", function($scope,$http,$rootScope){
	$scope.loadParceiros = function (){
		$http.get('php_files/parceiros.php').then(function success(data){
			console.log(data.data);
			$scope.parceiros=data.data;
		});
	}
	$scope.loadParceiros();
	$scope.funcShowForm = function (parceiro){
		$scope.showForm=parceiro.id;
		$scope.editP=parceiro;
	}
	$scope.submitEditParceiro = function (data){
		var files = document.getElementById('image').files[0];
		if(files){
			var reader = new FileReader();
			reader.readAsDataURL(files);
			reader.onload = function () {
				data.image=reader.result;
  				$http.post('php_files/edit_parceiro.php', data).then(function success(data){
  					alert('alterado com sucesso');
  					$scope.showForm=0;
  				});
			};
			reader.onerror = function (error) {
				alert('Ocorreu um erro!');
  				$scope.showForm=0;
		   	};
		}else{
  			$http.post('php_files/edit_parceiro.php', data).then(function success(data){
  				alert('alterado com sucesso');
  				$scope.showForm=0;
  			});
  		}
	}
	$scope.submitNewParceiro = function (data){
		var files = document.getElementById('image2').files[0];
		if(files){
			var reader = new FileReader();
			reader.readAsDataURL(files);
			reader.onload = function () {
				data.image=reader.result;
  				$http.post('php_files/new_parceiro.php', data).then(function success(data){
  					alert('Criado com sucesso');
  					$scope.showForm=0;
  					$scope.loadParceiros();
  					delete $scope.newP;
  				});
			};
			reader.onerror = function (error) {
				alert('Ocorreu um erro!');
  				$scope.showForm=0;
		   	};
		}else{
  			$http.post('php_files/new_parceiro.php', data).then(function success(data){
  				alert('Criado com sucesso');
  				$scope.showForm=0;
  				$scope.loadParceiros();
  				delete $scope.newP;
  			});
  		}
	}
})
.controller("ProgramacaoCtrl", function($scope,$http){
	$scope.dia = '';
	$scope.loadProgramas = function (){$scope.programas = [];
		$http.get('php_files/programas.php').then(function success(data){
			$scope.programas=data.data;
			angular.forEach($scope.programas, function (value,key){
				value['dias']=JSON.parse(value.dias);
				var array = value.dias;
				$scope.programas[key].diasa=value.dias;
				$scope.programas[key].dias={};
				angular.forEach(array, function (valuee,keyy){
					$scope.programas[key].dias[valuee]=true;
					console.log(valuee);
				});
				
			});
		});
	}
	$scope.loadProgramas();
	$scope.setDia = function (dia){
		$scope.dia=dia;
	}
	$scope.funcShowForm = function (data){
		$scope.showForm = data.id;
		$scope.editP={};
		$scope.editP.programa=data.programa;
		$scope.editP.id=data.id;
		$scope.editP.dias=data.dias;
		console.log($scope.editP.dias);
		$scope.editP.loc=JSON.parse(data.loc);
		console.log($scope.editP.loc);
		$scope.editP.inicio=new Date(data.inicio*1000 + 7200000);
		$scope.editP.fim=new Date(data.fim*1000 + 7200000);
		console.log($scope.editP.fim);
		console.log(data);
	}
	$scope.newP={};
	$scope.newP.loc=false;
	$scope.submitPrograma = function (data){
		var dia = data.dias;
		delete data.dias;
		data.dias=[];
		angular.forEach(dia, function (value,key){
			if(value==true){data.dias.push(key);}
		});
		delete $scope.showForm;
		data.inicio=new Date(data.inicio).getTime()/1000 - 7200;
		data.fim=new Date(data.fim).getTime()/1000 - 7200;
		console.log(data.inicio);
		console.log(data.fim);
		var files = document.getElementById('image').files[0];
		if (!files) {
			delete $scope.showForm;
			$http.post('php_files/set_programa.php', data).then(function success(data){
				alert('Alteração concluida com sucesso!');
				delete $scope.showForm;
				$scope.loadProgramas();
				console.log(data.data);
			});
  		}else{
  			var reader = new FileReader();
			reader.readAsDataURL(files);
			reader.onload = function () {
				data.image=reader.result;
				alert('Aguarde! A imagem está sendo processada!');
  				$http.post('php_files/set_programa.php', data).then(function success(data){
  					alert('Alteração concluida com sucesso!');
  					delete $scope.showForm;
  					$scope.loadProgramas();
  					console.log(data.data);
  				});
			};
			reader.onerror = function (error) {
				alert('Ocorreu um erro!');
  				delete $scope.showForm;
  				$scope.loadProgramas();
		   };
  		}
	}
	$scope.submitNewPrograma = function (data){
		var dia = data.dias;
		delete data.dias;
		data.dias=[];
		angular.forEach(dia, function (value,key){
			if(value==true){data.dias.push(key);}
		});
		delete $scope.newP;
		data.inicio=new Date(data.inicio).getTime()/1000 - 7200;
		data.fim=new Date(data.fim).getTime()/1000 - 7200;
		console.log(data);
		var files = document.getElementById('image2').files[0];
		if (!files) {
			delete $scope.showForm;
			$http.post('php_files/new_programa.php', data).then(function success(data){
				alert('Alteração concluida com sucesso!');
				$scope.loadProgramas();
				console.log(data.data);
			});
  		}else{
  			var reader = new FileReader();
			reader.readAsDataURL(files);
			reader.onload = function () {
				data.image=reader.result;
				alert('Aguarde! A imagem está sendo processada!');
  				$http.post('php_files/new_programa.php', data).then(function success(data){
  					alert('Alteração concluida com sucesso!');
  					$scope.loadProgramas();
  					console.log(data.data);
  				});
			};
			reader.onerror = function (error) {
				alert('Ocorreu um erro!');
  				$scope.loadProgramas();
		   };
  		}
	}
	$scope.deletePrograma = function (id){
		if(confirm('Deseja mesmo excluir?')){
			$http.post('php_files/delete_programa.php', {'id':id}).then(function success(data){
				alert('Excluido com sucesso!');
				$scope.loadProgramas();
			});
		}
	}
	$scope.diaChange = function (event){
		console.log(event);
	}
})
.filter('joinBy', function () {
    return function (input,delimiter) {
    	input = JSON.parse(input);
    	return input.join(delimiter);
    };
})
.controller("SlideCtrl", function($scope,$http){
	$scope.loadSlides = function (){
		$http.get('php_files/slide.php').then(function success(data){
			$scope.slides=data.data;
		});
	}
	$scope.loadSlides();
	$scope.submitNewSlide = function (data){
		var files = document.getElementById('image2').files[0];
		var reader = new FileReader();
		reader.readAsDataURL(files);
		reader.onload = function () {
			data.image=reader.result;
			alert('Aguarde! A imagem está sendo processada!');
  			$http.post('php_files/new_slide.php', data).then(function success(data){
  				alert('Slide inserido com sucesso!');
  				delete $scope.newP;
  				$scope.loadSlides();
  			});
		};
		reader.onerror = function (error) {
			alert('Ocorreu um erro!');
		};
	}
	$scope.funcShowForm = function (data){
		$scope.showForm = data.id;
		$scope.editS=data;
	}
	$scope.submitEditSlide = function (data){
		var files = document.getElementById('image2').files[0];
		if(!files){
			$http.post('php_files/edit_slide.php', data).then(function success(data){
  				alert('Slide editado com sucesso!');
  				delete $scope.editP;
  				delete $scope.showFormP;
  				$scope.loadSlides();
  			});
  			return 0;
		}
		var reader = new FileReader();
		reader.readAsDataURL(files);
		reader.onload = function () {
			data.image=reader.result;
			alert('Aguarde! A imagem está sendo processada!');
  			$http.post('php_files/edit_slide.php', data).then(function success(data){
  				alert('Slide editado com sucesso!');
  				delete $scope.editP;
  				delete $scope.showFormP;
  				$scope.loadSlides();
  			});
		};
		reader.onerror = function (error) {
			alert('Ocorreu um erro!');
		};
	}
	$scope.funcDeleteSlide = function (id){

		if(confirm('Deseja mesmo excluir o slide de n°'+id+' ?')){$http.post('php_files/delete_slide.php', id).then(function success(data){
			$scope.loadSlides();
			alert('Slide excluido com sucesso');
		})};
	}
})
.controller("PedidosCtrl", function($scope,$http){
	$scope.loadPedidos=function(){
		$http.get('php_files/pedidos.php').then(function success(data){
			$scope.pedidos=data.data;
			angular.forEach($scope.pedidos, function(value,key){
				$scope.pedidos[key].author=JSON.parse(value.author);
			});
		});
	}
	$scope.loadPedidos();
	$scope.deletePedido = function (id,index){
		delete $scope.pedidos[index];
		$http.post('php_files/delete_pedido.php', id).then(function success(data){
			$scope.loadPedidos();
		});
	}
});




;
function changeImage(event){
	document.getElementById('src_image').src=URL.createObjectURL(event.target.files[0]);
}
function changeImage2(event){
	document.getElementById('src_image2').src=URL.createObjectURL(event.target.files[0]);
}