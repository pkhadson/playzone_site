<!DOCTYPE html>
<html ng-app="pz">
<head>
	<meta charset="utf-8" />
	<title>Painel Administrável</title>
	<link rel="stylesheet" type="text/css" href="css/login.css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>
</head>
<body ng-controller="LoginCtrl">
<div class="box-login">
	<div class="header">Faça login</div>
	<form class="form" ng-submit="funcLogin(login)">
		<div class="input-icon"></div><input ng-model="login.login" type="number" placeholder="Digite seu ID" />
		<div class="input-icon"></div><input ng-model="login.pass" type="password" placeholder="Digite sua senha" />
		<button>Logar</button>
	</form>
</div>
<div class="footer">
	©  <a href="//fb.com/pkhadson" target="_blank">Patrick Hadson</a>
</div>

<script type="text/javascript" src="js/app-login.js"></script>
<script type="text/javascript" src="js/controller-login.js"></script>
</body>
</html>