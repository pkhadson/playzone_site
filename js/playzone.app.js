var app = angular.module("pz",['ngRoute','ngAnimate','hj.audio']);
app.controller("TopCtrl", function ($scope, $interval, $timeout, $http,$location, $rootScope){
	$scope.path=$location.absUrl();
	var message=0;
	$interval(function(){
		if(message == document.getElementsByClassName('message-tp').length-1){
			message=-1;
		}
		message+=300;
		$timeout(function(){
			message-=299;
		},1000);
	},6000);
	$scope.funcMessages = function (n){
		if(message == n){
			return true;
		}else{
			return false;
		}
	};
	$http.get('php_files/frases_top.php').then(function success(data){
		$scope.messages=data.data;
		console.log('"FRASES DO TOPO" carregado(a) com sucesso!');
	});
	$http.get('php_files/no_ar.php').then(function success(data){
		console.log('"NO AR" carregado(a) com sucesso!');
		$scope.noar=data.data;
	});
	$interval(function(){
		$http.get('php_files/music.php').then(function success(data){
			var music_a = $('.music_a').text();
			if(data.data!=music_a){
				changeMusic(data.data);
				$('.music_a').text(data.data);
			}
		});
	},3000);
	$scope.submitForm = function(data, t){
		$scope.popUpShow = 0;
		if(t==1){
			data.t='frases_listen';
		}
		if(t==2){
			data.t='pedidos';
		}

		$http.post('php_files/form_submit.php', data).then(function success(data){
			if(t==1){
				data.t='frases_listen';
			}
			if(t==2){
				data.t='pedidos';
			}
			$scope.funcPopUpShow(0);
			if(data.data!='true'){
				alert_pz('LightCoral', 	'Você deve ter um intervalo de no minimo 5 minutos entre seus pedidos');
				return 0;
			}
			if(t==1){
				alert_pz('DarkSeaGreen', 'Sua mensagem foi enviada para nossos administradores para ser avaliada');
			}
			if(t==2){
				alert_pz('DarkSeaGreen', 'Seu pedido foi enviado para o locutor');
			}
		});
	}
	$scope.popUpShow=0;
	$rootScope.funcPopUpShow = function (data){
		$scope.popUpShow=data;
	}
})
.controller("ListensMessagesCtrl", function ($scope, $interval, $timeout, $http, $rootScope){
	var message=0;
	$interval(function(){
		if(message == document.getElementsByClassName('lm').length-1){
			message=-1;
		}
		message+=300;
		$timeout(function(){
			message-=299;
		},600);
	},6000);
	$scope.funcMessages = function (n){
		if(message == n){
			return true;
		}else{
			return false;
		}
	};
	$http.get('php_files/frases_listen.php').then(function success(data){
		$rootScope.messages_l=data.data;
	});
}).config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
  $routeProvider
  .when('/', {
    templateUrl: 'views/home.html',
    controller:"HomeCtrl"
  })
  .when('/sobre', {
    templateUrl: 'views/sobre.html',
    controller:"SobreCtrl"
  })
  .when('/playlist', {
    templateUrl: 'views/playlist.html',
    controller:"PlayListCtrl"
  })
  .when('/programacao', {
    templateUrl: 'views/programacao.html',
    controller:"ProgramacaoCtrl"
  })
  .when('/contato', {
    templateUrl: 'views/contato.html',
    controller:"ContatoCtrl"
  }).otherwise({redirectTo: '/'});
}).run(function($rootScope, $window, $location){
	$rootScope.location=$location;
})
.filter('urlencode', [function() {
  var a = window.encodeURIComponent;
  return a;
}]);

$(document).ready(function() {
 
  $("#slide_pz").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      autoPlay: 4000,
      jsonPath : "php_files/slide.php" 
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
  $(".ss").click(function (event) {
    event.preventDefault();
    var idElemento = $(this).attr("href");
    var deslocamento = $(idElemento).offset().top;
    $('html, body').animate({ scrollTop: deslocamento }, 'slow');
  });
});
function changeMusic(data){
	$('.playing-now .music').animate({opacity:0}, 1000);
	setTimeout(function (){	
		if(data.length>20){
			data='<marquee>'+data+'</marquee>';
		}
		$('.playing-now .music').html(data).animate({opacity:1}, 1000);
	},1000);
}