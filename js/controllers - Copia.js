app.controller("HomeCtrl", function($scope,$location,$routeParams,$http,$rootScope){
	$scope.mais_tocadas = [
		{"music":"Sia", "artist":"Chandelier", "preview":"http://a859.phobos.apple.com/us/r30/Music6/v4/68/34/f1/6834f1f8-8fdb-4247-492a-c0caea580082/mzaf_3920281300599106672.plus.aac.p.m4a"},
		{"music":"Hello", "artist":"Adele", "preview":"http://a859.phobos.apple.com/us/r30/Music6/v4/68/34/f1/6834f1f8-8fdb-4247-492a-c0caea580082/mzaf_3920281300599106672.plus.aac.p.m4a"},
		{"music":"Thinking Out Loud", "artist":"Ed Sheeran", "preview":"http://a859.phobos.apple.com/us/r30/Music6/v4/68/34/f1/6834f1f8-8fdb-4247-492a-c0caea580082/mzaf_3920281300599106672.plus.aac.p.m4a"},
		{"music":"I’m Not The Only One", "artist":"Sam Smith", "preview":"http://a859.phobos.apple.com/us/r30/Music6/v4/68/34/f1/6834f1f8-8fdb-4247-492a-c0caea580082/mzaf_3920281300599106672.plus.aac.p.m4a"},
		{"music":"Wiggle feat. Snoop Dogg", "artist":"Jason Derulo", "preview":"http://a859.phobos.apple.com/us/r30/Music6/v4/68/34/f1/6834f1f8-8fdb-4247-492a-c0caea580082/mzaf_3920281300599106672.plus.aac.p.m4a"},
		{"music":"Am I Wrong", "artist":"Nico & Vinz", "preview":"http://a859.phobos.apple.com/us/r30/Music6/v4/68/34/f1/6834f1f8-8fdb-4247-492a-c0caea580082/mzaf_3920281300599106672.plus.aac.p.m4a"},
		{"music":"Clean Bandit", "artist":"Real Love", "preview":"http://a859.phobos.apple.com/us/r30/Music6/v4/68/34/f1/6834f1f8-8fdb-4247-492a-c0caea580082/mzaf_3920281300599106672.plus.aac.p.m4a"}
	];
	$http.get('php_files/mais_tocadas.php').then(function success(data){
		$scope.mais_tocadas=data.data;
	});
	$http.get('php_files/top_nacional.php').then(function success(data){
		$scope.top_nacional=data.data;
	});
	$scope.loadEnquete= function (){
		$http.get('php_files/enquete.php').then(function success(data){
			$scope.enquete=data.data;
			$scope.enqueteTotal=0;
			angular.forEach($scope.enquete.resultado, function(value,key){
				$scope.enqueteTotal+=value.votos;
			});
		});
	};
	$http.get('php_files/programa_mes.php').then(function success(data){
		$scope.programa_mes=data.data;
	});
	$http.get('php_files/parceiros.php').then(function success(data){
		$scope.parceiros=data.data;
	});
	$scope.previewM = function (state, src){
		var audio = document.getElementById('preview');

		if($scope.preview==src && audio.played){
			audio.pause();
			$scope.preview = '';
		}else{
			$scope.preview=src;
			audio.src=src;
			audio.play();
		}
	};
	$scope.loadEnquete();
	$scope.votoEnquete = function (opcao, index){
		$http.post('php_files/votar.php', {"id":index,"id_e":1}).then(function success(data){
			if(data.data.stat==false){
				alert_pz('LightCoral','Você já votou nessa enquete nas ultimas 100 horas!');
				$scope.loadEnquete();
			}
			if(data.data.stat==true){
				$scope.enquete.resultado[index].votos++;
				$scope.enqueteTotal++;
				alert_pz('DarkSeaGreen','Seu voto foi valido');
				$scope.loadEnquete();
			}
		});
	};
	$scope.checkPreview=function (music){
		if($scope.preview == 'preview/music/'+music.music+' - '+music.artist && !document.getElementById('preview').paused){
			return 1;
		}else{
			return 0;
		}
	};
})
.controller("SobreCtrl", function($scope,$location,$http){
	$http.get('php_files/page_sobre.php').then(function success(data){
		document.getElementsByClassName('sobre')[0].innerHTML = data.data;
	});
})
.controller("PlayListCtrl", function($scope,$location,$http){
	$scope.loadPlaylist = function (){
		$http.get('php_files/playlist.php').then(function success(data){
			$scope.playlist=data.data;
			var i =0;
			angular.forEach($scope.playlist, function (value,key){
				$scope.playlist[i]['title']=value.title.split(' - ');
				i++;
			});
		});
	};
	$scope.loadPlaylist();
})
.controller("ProgramacaoCtrl", function($scope,$location,$http){
	$http.get('php_files/programacao.php').then(function success(data){
		$scope.programacao=data.data;
	});
	var now = new Date();
	var week_start=new Date((now.getTime()/1000) - (now.getDay() *  86400)*1000);
	$scope.dias=[
		{'time':week_start*1,'nome':'Segunda','abreviacao':'Seg'},
		{'time':week_start*2,'nome':'Terça','abreviacao':'Ter'},
		{'time':week_start*3,'nome':'Quarta','abreviacao':'Qua'},
		{'time':week_start*4,'nome':'Quinta','abreviacao':'Qui'},
		{'time':week_start*5,'nome':'Sexta','abreviacao':'Sex'},
		{'time':week_start*6,'nome':'Sabado','abreviacao':'Sab'},
		{'time':week_start*7,'nome':'Domingo','abreviacao':'Dom'}
	];
	angular.forEach($scope.dias, function(value,key){
		if(value.time/week_start<now.getDay()){
			$scope.dias[key].time=value.time+(604800000*1000);
		}
	});
	$scope.date = new Date();
	function timeDay(n){

	};
})
.controller("ContatoCtrl", function($scope,$location,$http){
	$scope.funcContato = function (form) {
		$http.post('php_files/post_contato.php', form).then(function success(data){
			if(data.data=='true'){
				alert_pz('DarkSeaGreen','Enviado com sucesso! Você recebera uma resposta em breve');
			}else{
				alert_pz('LightCoral','Você só pode enviar um email por dia');
			}
		});
	}
});


function alert_pz(color, text){
	var objeto = document.getElementById('bar-alert');
	objeto.innerHTML=text;
	objeto.className += 'active';
	objeto.style.background = color;
	setTimeout(function(){
		objeto.classList.remove('active');
	}, 3000);
};