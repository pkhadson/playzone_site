app.controller("HomeCtrl", ['$scope','$http', function($scope,$http){
	$http.get('php_files/mais_tocadas.php').then(function success(data){
		$scope.mais_tocadas=data.data;
		$scope.loadTopNacional();
		console.log('"MAIS TOCADAS" carregado(a) com sucesso!');
	});
	$scope.loadTopNacional = function (){
		$http.get('php_files/top_nacional.php').then(function success(data){
			$scope.top_nacional=data.data;
			$scope.loadEnquete();
			console.log('"TOP NACIONAL" carregado(a) com sucesso!');
		});
	}
	$scope.loadEnquete= function (){
		$http.get('php_files/enquete.php').then(function success(data){
			$scope.loadProgramaMes();
			console.log('"ENQUETE" carregado(a) com sucesso!');	
			$scope.enquete=data.data;
			$scope.enqueteTotal=0;
			angular.forEach($scope.enquete.resultado, function(value,key){
				$scope.enqueteTotal+=value.votos;
			});
		});
	};
	$scope.loadProgramaMes = function (){
		$http.get('php_files/programa_mes.php').then(function success(data){
			console.log('"PROGRAMA DO MÊS" carregado(a) com sucesso!');	
			$scope.programa_mes=data.data;
		});
	}
	$scope.loadParceiros= function (){
		$http.get('php_files/parceiros.php').then(function success(data){
			console.log('"PARCEIROS" carregado(a) com sucesso!');	
			$scope.parceiros=data.data;
		});
	}
	$scope.previewM = function (state, src){
		var audio = document.getElementById('preview');

		if($scope.preview==src && audio.played){
			audio.pause();
			$scope.preview = '';
		}else{
			$scope.preview=src;
			audio.src=src;
			audio.play();
		}
	};
	$scope.votoEnquete = function (opcao, index){
		$http.post('php_files/votar.php', {"id":index,"id_e":1}).then(function success(data){
			if(data.data.stat==false){
				alert_pz('LightCoral','Você já votou nessa enquete nas ultimas 100 horas!');
				$scope.loadEnquete();
				console.log('Voto não computado!');
			}
			if(data.data.stat==true){
				$scope.enquete.resultado[index].votos++;
				$scope.enqueteTotal++;
				alert_pz('DarkSeaGreen','Seu voto foi valido');
				$scope.loadEnquete();
				console.log('Voto computado!')
				return 1;
			}
			alert_pz('LightCoral','Você já votou nessa enquete nas ultimas 100 horas!');
		});
	};
	$scope.checkPreview=function (music){
		if($scope.preview == 'preview/music/'+music.music+' - '+music.artist && !document.getElementById('preview').paused){
			return 1;
		}else{
			return 0;
		}
	};
}])
.controller("SobreCtrl", ['$scope','$http', function($scope,$http){
	$http.get('php_files/page_sobre.php').then(function success(data){
		document.getElementsByClassName('sobre')[0].innerHTML = data.data;
	});
}])
.controller("PlayListCtrl", ['$scope','$http', function($scope,$http){
	$scope.loadPlaylist = function (){
		$http.get('php_files/playlist.php').then(function success(data){
			$scope.playlist=data.data;
			var i =0;
			angular.forEach($scope.playlist, function (value,key){
				$scope.playlist[i]['title']=value.title.split(' - ');
				i++;
			});
		});
	};
	$scope.loadPlaylist();
}])
.controller("ProgramacaoCtrl", ['$scope','$http', function($scope,$http){
	$http.get('php_files/programacao.php').then(function success(data){
		$scope.programacao=data.data;
	});
	var now = new Date();
	var week_start=new Date((now.getTime()/1000) - (now.getDay() *  86400)*1000);
	$scope.dias=[
		{'time':week_start*1,'nome':'Segunda','abreviacao':'Seg'},
		{'time':week_start*2,'nome':'Terça','abreviacao':'Ter'},
		{'time':week_start*3,'nome':'Quarta','abreviacao':'Qua'},
		{'time':week_start*4,'nome':'Quinta','abreviacao':'Qui'},
		{'time':week_start*5,'nome':'Sexta','abreviacao':'Sex'},
		{'time':week_start*6,'nome':'Sabado','abreviacao':'Sab'},
		{'time':week_start*7,'nome':'Domingo','abreviacao':'Dom'}
	];
	angular.forEach($scope.dias, function(value,key){
		if(value.time/week_start<now.getDay()){
			$scope.dias[key].time=value.time+(604800000*1000);
		}
	});
	$scope.date = new Date();
	function timeDay(n){

	};
}])
.controller("ContatoCtrl", ['$scope','$http', function($scope,$http){
	$scope.funcContato = function (form) {
		$http.post('php_files/post_contato.php', form).then(function success(data){
			if(data.data=='true'){
				alert_pz('DarkSeaGreen','Enviado com sucesso! Você recebera uma resposta em breve');
			}else{
				alert_pz('LightCoral','Você só pode enviar um email por dia');
			}
		});
	}
}]);


function alert_pz(color, text){
	var objeto = document.getElementById('bar-alert');
	objeto.innerHTML=text;
	objeto.className += 'active ';
	objeto.style.background = color;
	setTimeout(function(){
		objeto.classList.remove('active');
	}, 3000);
};